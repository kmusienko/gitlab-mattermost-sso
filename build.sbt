name := "mattermost-sso"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  "org.scalaj" %% "scalaj-http" % "2.4.1",
  "com.typesafe.play" %% "play-ws-standalone-json" % "2.0.0-M6",
  "com.typesafe" % "config" % "1.3.3"
)

assemblyJarName in assembly := "setup-sso.jar"