package validators

import scala.util.{Failure, Try}

object EnvValidator {
  def withEnvValidation[T](env: String)(f: => Try[T]): Try[T] = {
    if (List("local", "dev", "stage", "prod").contains(env)) f
    else Failure(new IllegalArgumentException("Unknown environment!"))
  }
}