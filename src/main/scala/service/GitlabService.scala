package service

import config.GitlabConfig

import scalaj.http.{Http, HttpResponse}

class GitlabService(gitlabConfigService: GitlabConfig) {

  def signIn: HttpResponse[String] = {
    println("About to sign in on Gitlab as root...")
    val signInPageResponse = Http(s"http://${gitlabConfigService.url}/users/sign_in").asString
    val signInResponse: HttpResponse[String] = Http(s"http://${gitlabConfigService.url}/users/sign_in")
      .postForm {
        Seq(
          "authenticity_token" -> extractValueByPattern(fromBody = signInPageResponse.body, gitlabConfigService.tokenPatternStart, gitlabConfigService.tokenPatternEnd),
          "user[login]" -> gitlabConfigService.rootUsername,
          "user[password]" -> gitlabConfigService.rootPassword,
          "user[remember_me]" -> "0"
        )
      }.cookies(signInPageResponse.cookies).asString
    println(s"Signing in finished. Result: [code: ${signInResponse.code}]")
    signInResponse
  }

  def createApplication(signInResponse: HttpResponse[String]): (String, String) = {
    println("About to create an application on Gitlab...")
    val newApplicationPageResponse: HttpResponse[String] =
      Http(s"http://${gitlabConfigService.url}/admin/applications/new").cookies(signInResponse.cookies).asString

    val createApplicationResponse: HttpResponse[String] = Http(s"http://${gitlabConfigService.url}/admin/applications")
      .postForm {
        Seq(
          "authenticity_token" -> extractValueByPattern(fromBody = newApplicationPageResponse.body, gitlabConfigService.tokenPatternStart, gitlabConfigService.tokenPatternEnd),
          "doorkeeper_application[name]" -> "mattermost",
          "doorkeeper_application[redirect_uri]" -> gitlabConfigService.redirectUri,
          "doorkeeper_application[trusted]" -> "0",
          "doorkeeper_application[trusted]" -> "1",
          "doorkeeper_application[scopes][]" -> "api",
          "doorkeeper_application[scopes][]" -> "read_user"
        )
      }.cookies(signInResponse.cookies).asString

    println(s"Creation of an application finished. Result: [${createApplicationResponse.code}]")

    val applicationUrl: String = createApplicationResponse.headers("Location").head
    val applicationPageResponse = Http(applicationUrl).cookies(signInResponse.cookies).asString

    val appId = extractValueByPattern(fromBody = applicationPageResponse.body, valuePatternStart = gitlabConfigService.appIdPatternStart, valuePatternEnd = gitlabConfigService.appIdPatternEnd)
    val secret = extractValueByPattern(fromBody = applicationPageResponse.body, valuePatternStart = gitlabConfigService.secretPatternStart, valuePatternEnd = gitlabConfigService.secretPatternEnd)

    println(s"Extracted the appId and secret from the application page response. [AppId: $appId, Secret: $secret]")
    (appId, secret)
  }

  def extractValueByPattern(fromBody: String, valuePatternStart: String, valuePatternEnd: String): String = {
    val valueStartIndex = fromBody.indexOf(valuePatternStart)
    fromBody.substring(
      valueStartIndex + valuePatternStart.length,
      fromBody.indexOf(valuePatternEnd, valueStartIndex)
    )
  }
}