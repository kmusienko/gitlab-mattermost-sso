package service

import config.{GitlabConfig, MattermostConfig}
import play.api.libs.json._

import scalaj.http.{Http, HttpResponse}

class MattermostService(mattermostConfigService: MattermostConfig,
                        gitlabConfigService: GitlabConfig) {

  def signUp: HttpResponse[String] = {
    println("About to sign up on Mattermost as admin...")
    val signUpPageResponse = Http(s"http://${mattermostConfigService.url}/signup_email").asString
    val data = Json.obj(
      "email" -> mattermostConfigService.adminEmail,
      "username" -> mattermostConfigService.adminUsername,
      "password" -> mattermostConfigService.adminPassword
    )
    val signUpResponse: HttpResponse[String] = Http(s"http://${mattermostConfigService.url}/api/v4/users")
      .postData(data.toString)
      .header("content-type", "application/json")
      .cookies(signUpPageResponse.cookies).asString
    println(s"Signing up finished. Result: [${signUpResponse.code}]")
    signUpResponse
  }

  def signIn(signUpResponse: HttpResponse[String]): HttpResponse[String] = {
    println("About to sign in on Mattermost as admin...")
    val loginData = Json.obj(
      "device_id" -> "",
      "id" -> (Json.parse(signUpResponse.body) \ "id").as[String],
      "password" -> "admin",
      "token" -> ""
    )
    val signInResponse: HttpResponse[String] = Http(s"http://${mattermostConfigService.url}/api/v4/users/login")
      .postData(loginData.toString)
      .header("content-type", "application/json").asString
    println(s"Signing in finished. Result: [${signInResponse.code}]")
    signInResponse
  }

  def setupGitlabApplication(signInResponse: HttpResponse[String], id: String, secret: String): HttpResponse[String] = {
    println("About to setup the Gitlab application on Mattermost...")
    val token = signInResponse.headers("Token").head
    val currentConfigResponse = Http(s"http://${mattermostConfigService.url}/api/v4/config")
      .headers(Seq(("Authorization", s"Bearer $token")))
      .cookies(signInResponse.cookies).asString

    val mattermostConfig: JsObject = Json.parse(currentConfigResponse.body).as[JsObject]
    val newGitlabSettings = JsObject(
      List(
        ("GitLabSettings", JsObject(
          List(
            ("AuthEndpoint", JsString(gitlabConfigService.authEndpoint)),
            ("Enable", JsBoolean(true)),
            ("id", JsString(id)),
            ("Scope", JsString("")),
            ("Secret", JsString(secret)),
            ("TokenEndpoint", JsString(gitlabConfigService.tokenEndpoint)),
            ("UserApiEndpoint", JsString(gitlabConfigService.userApiEndpoint))
          )
        ).as[JsValue])
      )
    )
    val updatedMattermostConfig: JsValue = mattermostConfig.deepMerge(newGitlabSettings)

    val updateConfigResponse = Http(s"http://${mattermostConfigService.url}/api/v4/config")
      .put(updatedMattermostConfig.toString)
      .headers(Seq(("Authorization", s"Bearer $token"), ("content-type", "application/json")))
      .cookies(signInResponse.cookies).asString
    println(s"Setting up the Gitlab application on Mattermost finished. Result: [${updateConfigResponse.code}]")
    updateConfigResponse
  }
}