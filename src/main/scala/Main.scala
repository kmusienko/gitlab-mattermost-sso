import com.typesafe.config.ConfigFactory
import config.{GitlabConfig, MattermostConfig}
import service.{GitlabService, MattermostService}
import validators.EnvValidator.withEnvValidation

import scala.util.{Failure, Success, Try}

object Main extends App {

  args.headOption match {
    case Some(env) =>
      withEnvValidation(env) {
        Try {
          val config = ConfigFactory.load()
          val gitlabConfig = new GitlabConfig(config, env)
          val mattermostConfig = new MattermostConfig(config, env)
          val gitlabService = new GitlabService(gitlabConfig)
          val mattermostService = new MattermostService(mattermostConfig, gitlabConfig)

          val gitlabSignInResponse = gitlabService.signIn
          val (id, secret) = gitlabService.createApplication(gitlabSignInResponse)
          val mattermostSignUpResponse = mattermostService.signUp
          val mattermostSignInResponse = mattermostService.signIn(mattermostSignUpResponse)
          mattermostService.setupGitlabApplication(mattermostSignInResponse, id, secret)
        }
      } match {
        case Success(_) => println("The script has been successfully executed")
        case Failure(error) => println(s"Failed to execute the script. Reason: [${error.getMessage}]")
      }
    case _ => println("Please, provide the environment on which you want to execute the script")
  }
}