package config

import com.typesafe.config.Config

class MattermostConfig(config: Config, env: String) {

  val url: String = config.getString(s"$env.mattermost.url")

  val adminEmail: String = config.getString(s"$env.mattermost.users.admin.email")

  val adminUsername: String = config.getString(s"$env.mattermost.users.admin.username")

  val adminPassword: String = config.getString(s"$env.mattermost.users.admin.password")
}