package config

import com.typesafe.config.Config

class GitlabConfig(config: Config, env: String) {

  val url: String = config.getString(s"$env.gitlab.url")

  val rootUsername: String = config.getString(s"$env.gitlab.users.root.username")

  val rootPassword: String = config.getString(s"$env.gitlab.users.root.password")

  val redirectUri: String = config.getString(s"$env.gitlab.redirectUri")

  val authEndpoint: String = config.getString(s"$env.gitlab.authEndpoint")

  val tokenEndpoint: String = config.getString(s"$env.gitlab.tokenEndpoint")

  val userApiEndpoint: String = config.getString(s"$env.gitlab.userApiEndpoint")

  val tokenPatternStart: String = config.getString("csrf.tokenPatternStart")

  val tokenPatternEnd: String = config.getString("csrf.tokenPatternEnd")

  val appIdPatternStart: String = config.getString("applicationId.patternStart")

  val appIdPatternEnd: String = config.getString("applicationId.patternEnd")

  val secretPatternStart: String = config.getString("applicationSecret.patternStart")

  val secretPatternEnd: String = config.getString("applicationSecret.patternEnd")
}